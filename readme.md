Requirements
============
C++17 compiler
SDL 2
SDL_image 2

How to build
============
Run the following commands in this folder:

mkdir build

cd build

cmake ..

cmake --build .

The generated program, Timer_Stopwatch_Clock, will be located in the build folder. This program **must** be moved up a directory and placed beside the Assets folder for the textures and audio to work. This can be done with the following command, assuming you are still in the build directory:

mv Timer_Stopwatch_Clock ..


How to run
==========
This program has four different modes: a counter, clock, stopwatch and timer mode. The mode can be selected by giving the required mode as the first argument to the program. When using the timer mode, it can be given a time, in seconds, which the timer will begin at. If the time value is missing, a time of 100 seconds will be selected instead. The stopwatch can be started and stopped using the space bar.

Usage examples:

./Timer_Stopwatch_Clock

./Timer_Stopwatch_Clock stopwatch

./Timer_Stopwatch_Clock clock

./Timer_Stopwatch_Clock timer 50

Assets
======
Image: https://commons.wikimedia.org/wiki/File:7_Segment_Display_with_Labeled_Segments.svg
Audio: https://freesound.org/people/ZyryTSounds/sounds/219244/