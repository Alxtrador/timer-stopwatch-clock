#include "app.h"

#include <array>
#include <iostream>
#include <vector>

#include "SDL2/SDL.h"
#include "SDL2/SDL_video.h"
#include "SDL2/SDL_image.h"

#include "clock.h"
#include "counter.h"
#include "stopwatch.h"
#include "timer.h"

App::App(int width, int height, Mode mode, int timer_count)
    : width_(width), height_(height), mode_(mode), 
      window_(SDL_CreateWindow("Timer", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, width, height, 0)),
      renderer_(SDL_CreateRenderer(window_, -1, 0)),
      engine_(create_engine(mode, timer_count)) {
}

App::~App() {
    IMG_Quit();
    SDL_DestroyRenderer(renderer_);
    SDL_DestroyWindow(window_);
    SDL_Quit();
}

std::unique_ptr<AppEngine> App::create_engine(Mode mode, int timer_count) {
    switch (mode) {
        case eTimer:
            return std::make_unique<Timer>(renderer_, width_, timer_count);
        case eStopwatch:
            return std::make_unique<Stopwatch>(renderer_, width_, height_);
        case eClock:
            return std::make_unique<Clock>(renderer_, width_);
        case eCounter:
            return std::make_unique<Counter>(renderer_, width_);
    }
}

void App::run() {
    engine_->run(renderer_);
}
