#include "timer.h"

#include "SDL2/SDL.h"
#include "SDL2/SDL_video.h"
#include "SDL2/SDL_image.h"

#include <array>

#include "constants.h"

Timer::Timer(SDL_Renderer *renderer, int width, int count)
    : textures_(init_textures(renderer)), audio_data_(init_audio("Assets/Audio/alarm.wav")), 
      width_(width), count_(count) {}

Timer::~Timer() {
    for (SDL_Texture *texture: textures_) {
        SDL_DestroyTexture(texture);
    }
}

std::array<SDL_Texture*, 11> Timer::init_textures(SDL_Renderer *renderer) {
    return std::array<SDL_Texture*, 11>{
        IMG_LoadTexture(renderer, "Assets/Images/Zero.png"),
        IMG_LoadTexture(renderer, "Assets/Images/One.png"),
        IMG_LoadTexture(renderer, "Assets/Images/Two.png"),
        IMG_LoadTexture(renderer, "Assets/Images/Three.png"),
        IMG_LoadTexture(renderer, "Assets/Images/Four.png"),
        IMG_LoadTexture(renderer, "Assets/Images/Five.png"),
        IMG_LoadTexture(renderer, "Assets/Images/Six.png"),
        IMG_LoadTexture(renderer, "Assets/Images/Seven.png"),
        IMG_LoadTexture(renderer, "Assets/Images/Eight.png"),
        IMG_LoadTexture(renderer, "Assets/Images/Nine.png"),
        IMG_LoadTexture(renderer, "Assets/Images/Filled_dot.png")
    };
}

AudioData Timer::init_audio(std::string audio_file) {
    return AudioData(audio_file);
}

void Timer::draw(SDL_Renderer *renderer) {
    int digit_to_draw = count_;
    for (int i = 0; (i * DIGIT_WIDTH) < width_ * DIGIT_WIDTH; i++) {
        SDL_Texture *digit_texture;
        if (digit_to_draw != 0) {
            digit_texture = textures_[digit_to_draw % 10];
            digit_to_draw /= 10;
        } else {
            digit_texture = textures_[0];
        }
        SDL_Rect size;
        size.x = width_ - (DIGIT_WIDTH * (i + 1) + (i * DIGIT_SPACING));
        size.y = 0;
        size.w = DIGIT_WIDTH;
        size.h = DIGIT_HEIGHT;
        SDL_RenderCopy(renderer, digit_texture, NULL, &size);
    }
    SDL_RenderPresent(renderer);
}

void Timer::run(SDL_Renderer *renderer) {
    SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255);
    SDL_RenderClear(renderer);
    SDL_RenderPresent(renderer);

    bool quit = false;
    bool played_alarm = false;
    Uint64 current_time = SDL_GetTicks64();
    Uint64 target_time = current_time + 1000;
    while (!quit) {
        SDL_Event event;
        while (SDL_PollEvent(&event)) {
            switch (event.type) {
                case SDL_QUIT:
                    quit = true;
                    break;
                default:
                    break;
            }
        }

        if (!played_alarm && count_ == 0) {
            played_alarm = true;
            audio_data_.play_audio();
        }

        Uint64 passed_time = SDL_GetTicks64() - current_time;
        if (passed_time > 1000 && count_ != 0) {
            count_ -= 1;
            draw(renderer);
            target_time += 1000;
            current_time = SDL_GetTicks64();
        }

        //Introduce a short delay to avoid 100% usage of a cpu core
        SDL_Delay(1);
    }
}