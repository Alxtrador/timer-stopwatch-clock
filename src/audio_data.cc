#include "audio_data.h"

#include <string>

#include "SDL2/SDL.h"

AudioData::AudioData(std::string audio_file) {
    SDL_LoadWAV(audio_file.c_str(), &spec_, &audio_buf_, &audio_len_);
    device_id_ = SDL_OpenAudioDevice(NULL, 0, &spec_, NULL, 0);
}

AudioData::~AudioData() {
    SDL_CloseAudioDevice(device_id_);
    SDL_FreeWAV(audio_buf_);
}

void AudioData::play_audio() {
    SDL_QueueAudio(device_id_, audio_buf_, audio_len_);
    SDL_PauseAudioDevice(device_id_, 0);
}
