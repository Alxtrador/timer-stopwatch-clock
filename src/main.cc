#include "app.h"

#include <iostream>

int main(int argc, char *argv[]) {
    int width = 124 * 7 + 50;
    int height = 217;

    SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO | SDL_INIT_TIMER);

    Mode mode = eCounter;
    int timer_count = 100;

    if (argc == 1) {
        std::cout << "Starting up in counter mode.\n";
    } else {
        std::string user_mode = argv[1];
        if (user_mode == "counter") {
        } else if (user_mode == "clock") {
            mode = eClock;
        } else if (user_mode == "timer") {
            mode = eTimer;
            if (argc > 2) {
                try {
                    timer_count = std::stoi(argv[2]);
                    if (timer_count < 0) {
                        std::cout << "Timer value must be positive. Using default value of 100 instead.\n";
                        timer_count = 100;
                    }
                } catch (std::exception const &_e) {
                    std::cout << "Could not parse timer argument as integer. Using default value of 100 instead.\n";
                }
            } else {
                std::cout << "No time value passed so timer will use default value of 100.\n";
            }
        } else if (user_mode == "stopwatch") {
            mode = eStopwatch;
        } else {
            std::cout << user_mode << " is an invalid mode.\n";
            return -1;
        }
    }

    App app(width, height, mode, timer_count);
    app.run();

    return 0;
}
