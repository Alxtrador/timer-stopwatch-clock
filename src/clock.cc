#include "clock.h"

#include "SDL2/SDL.h"
#include "SDL2/SDL_video.h"
#include "SDL2/SDL_image.h"

#include <array>
#include <ctime>
#include <vector>

#include "constants.h"

Clock::Clock(SDL_Renderer *renderer, int width)
    : textures_(init_textures(renderer)), width_(width) {}

Clock::~Clock() {
    for (SDL_Texture *texture: textures_) {
        SDL_DestroyTexture(texture);
    }
}

std::array<SDL_Texture*, 11> Clock::init_textures(SDL_Renderer *renderer) {
    return std::array<SDL_Texture*, 11>{
        IMG_LoadTexture(renderer, "Assets/Images/Zero.png"),
        IMG_LoadTexture(renderer, "Assets/Images/One.png"),
        IMG_LoadTexture(renderer, "Assets/Images/Two.png"),
        IMG_LoadTexture(renderer, "Assets/Images/Three.png"),
        IMG_LoadTexture(renderer, "Assets/Images/Four.png"),
        IMG_LoadTexture(renderer, "Assets/Images/Five.png"),
        IMG_LoadTexture(renderer, "Assets/Images/Six.png"),
        IMG_LoadTexture(renderer, "Assets/Images/Seven.png"),
        IMG_LoadTexture(renderer, "Assets/Images/Eight.png"),
        IMG_LoadTexture(renderer, "Assets/Images/Nine.png"),
        IMG_LoadTexture(renderer, "Assets/Images/Filled_dot.png")
    };
}

void Clock::draw(SDL_Renderer *renderer, char *time) {
    //Apply texture for each character
    for (int i = 0; i < 6; i++) {
        char digit_char = time[i];
        int digit = digit_char - 48;
        SDL_Texture *digit_texture = textures_[digit];
        SDL_Rect size;
        size.x = DIGIT_WIDTH * i + (i * DIGIT_SPACING) + (COLON_SPACING * int(i / 2));
        size.y = 0;
        size.w = DIGIT_WIDTH;
        size.h = DIGIT_HEIGHT;
        SDL_RenderCopy(renderer, digit_texture, NULL, &size);

        //Create colon
        if (i % 2 == 1 && i != 5) {
            SDL_Texture *dot_texture = textures_[10];
            SDL_Rect colon_size_upper;
            colon_size_upper.x = 85 + DIGIT_WIDTH * i + (i * DIGIT_SPACING) + (COLON_SPACING * (int(i / 2)+ 1));
            colon_size_upper.y = 50;
            colon_size_upper.w = DOT_WIDTH;
            colon_size_upper.h = DOT_HEIGHT;
            SDL_RenderCopy(renderer, dot_texture, NULL, &colon_size_upper);
            SDL_Rect colon_size_lower;
            colon_size_lower.x = 85 + DIGIT_WIDTH * i + (i * DIGIT_SPACING) + (COLON_SPACING * (int(i / 2)+ 1));
            colon_size_lower.y = 150;
            colon_size_lower.w = DOT_WIDTH;
            colon_size_lower.h = DOT_HEIGHT;
            SDL_RenderCopy(renderer, dot_texture, NULL, &colon_size_lower);
        }
    }
    SDL_RenderPresent(renderer);
}

void Clock::run(SDL_Renderer *renderer) {
    SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255);
    SDL_RenderClear(renderer);
    SDL_RenderPresent(renderer);

    bool quit = false;
    while (!quit) {
        SDL_Event event;
        while (SDL_PollEvent(&event)) {
            switch (event.type) {
                case SDL_QUIT:
                    quit = true;
                    break;
                default:
                    break;
            }
        }

        std::time_t time = std::time(nullptr);
        char time_text[std::size("HHMMSS")];
        std::strftime(std::data(time_text), std::size(time_text), "%H%M%S", std::localtime(&time));
        Clock::draw(renderer, time_text);

        SDL_Delay(100);
    }
}
