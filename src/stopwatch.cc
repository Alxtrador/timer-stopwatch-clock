#include "stopwatch.h"

#include "SDL2/SDL.h"
#include "SDL2/SDL_video.h"
#include "SDL2/SDL_image.h"

#include <array>

#include "constants.h"

Stopwatch::Stopwatch(SDL_Renderer *renderer, int width, int height)
    : textures_(init_textures(renderer)), width_(width), height_(height) {}

Stopwatch::~Stopwatch() {
    for (SDL_Texture *texture: textures_) {
        SDL_DestroyTexture(texture);
    }
}

std::array<SDL_Texture*, 11> Stopwatch::init_textures(SDL_Renderer *renderer) {
    return std::array<SDL_Texture*, 11>{
        IMG_LoadTexture(renderer, "Assets/Images/Zero.png"),
        IMG_LoadTexture(renderer, "Assets/Images/One.png"),
        IMG_LoadTexture(renderer, "Assets/Images/Two.png"),
        IMG_LoadTexture(renderer, "Assets/Images/Three.png"),
        IMG_LoadTexture(renderer, "Assets/Images/Four.png"),
        IMG_LoadTexture(renderer, "Assets/Images/Five.png"),
        IMG_LoadTexture(renderer, "Assets/Images/Six.png"),
        IMG_LoadTexture(renderer, "Assets/Images/Seven.png"),
        IMG_LoadTexture(renderer, "Assets/Images/Eight.png"),
        IMG_LoadTexture(renderer, "Assets/Images/Nine.png"),
        IMG_LoadTexture(renderer, "Assets/Images/Filled_dot.png")
    };
}

void Stopwatch::draw(SDL_Renderer *renderer, Uint64 value) {
    Uint64 milliseconds = value % 1000;
    Uint64 seconds = (value / 1000) % 60;
    Uint64 minutes = (value / 1000) / 60;

    int millisecond_width = DIGIT_WIDTH / 2;
    int millisecond_height = DIGIT_HEIGHT / 2;
    //Draw milliseconds here
    for (int i = 0; i < 3; i++) {
        SDL_Texture *digit_texture;
        if (milliseconds != 0) {
            digit_texture = textures_[milliseconds % 10];
            milliseconds /= 10;
        } else {
            digit_texture = textures_[0];
        }
        SDL_Rect size;
        size.x = width_ - (millisecond_width * (i + 1) + (i * DIGIT_SPACING));
        size.y = height_ / 2;
        size.w = millisecond_width;
        size.h = millisecond_height;
        SDL_RenderCopy(renderer, digit_texture, NULL, &size);
    }
    
    int total_millisecond_width = millisecond_width * 3 + 3 * DIGIT_SPACING;
    //Draw seconds here
    int second_width = DIGIT_WIDTH;
    int second_height = DIGIT_HEIGHT;
    for (int i = 0; i < 2; i++) {
        SDL_Texture *digit_texture;
        if (seconds != 0) {
            digit_texture = textures_[seconds % 10];
            seconds /= 10;
        } else {
            digit_texture = textures_[0];
        }
        SDL_Rect size;
        size.x = width_ - ((second_width * (i + 1) + (i * DIGIT_SPACING)) + total_millisecond_width) - 10;
        size.y = 0;
        size.w = second_width;
        size.h = second_height;
        SDL_RenderCopy(renderer, digit_texture, NULL, &size);
    }

    int total_second_width = second_width * 2 + 2 * DIGIT_SPACING + total_millisecond_width;
    int colon_width = width_ - (total_second_width + 4 * DIGIT_SPACING);
    //Draw colon
    SDL_Texture *dot_texture = textures_[10];
    SDL_Rect colon_size_upper;
    colon_size_upper.x = colon_width;
    colon_size_upper.y = 50;
    colon_size_upper.w = DOT_WIDTH;
    colon_size_upper.h = DOT_HEIGHT;
    SDL_RenderCopy(renderer, dot_texture, NULL, &colon_size_upper);
    SDL_Rect colon_size_lower;
    colon_size_lower.x = colon_width;
    colon_size_lower.y = 150;
    colon_size_lower.w = DOT_WIDTH;
    colon_size_lower.h = DOT_HEIGHT;
    SDL_RenderCopy(renderer, dot_texture, NULL, &colon_size_lower);

    int total_colon_width = colon_width;
    //Draw minutes here
    //Let minutes take up as much remaining space as possible
    for (int i = 0; ((DIGIT_WIDTH * (i + 1) + (i * DIGIT_SPACING)) + (width_ - total_colon_width)) < width_; i++) {
        SDL_Texture *digit_texture;
        if (minutes != 0) {
            digit_texture = textures_[minutes % 10];
            minutes /= 10;
        } else {
            digit_texture = textures_[0];
        }
        SDL_Rect size;
        size.x = width_ - ((DIGIT_WIDTH * (i + 1) + (i * DIGIT_SPACING)) + (width_ - total_colon_width)) - 10;
        size.y = 0;
        size.w = DIGIT_WIDTH;
        size.h = DIGIT_HEIGHT;
        SDL_RenderCopy(renderer, digit_texture, NULL, &size);
    }

    SDL_RenderPresent(renderer);
}

void Stopwatch::run(SDL_Renderer *renderer) {
    SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255);
    SDL_RenderClear(renderer);
    SDL_RenderPresent(renderer);

    bool quit = false;
    bool stop = true;
    Uint64 start_time = SDL_GetTicks64();
    Uint64 time_elapsed = 0;
    while (!quit) {
        SDL_Event event;
        while (SDL_PollEvent(&event)) {
            switch (event.type) {
                case SDL_QUIT:
                    quit = true;
                    break;
                case SDL_KEYDOWN:
                    if (event.key.keysym.sym == SDLK_SPACE) {
                        stop = !stop;
                        if (!stop) {
                            start_time = SDL_GetTicks64();
                        }
                    }
                    break;
                default:
                    break;
            }
        }

        if (!stop) {
            time_elapsed += SDL_GetTicks64() - start_time;
            start_time = SDL_GetTicks64();
        }
        draw(renderer, time_elapsed);
    }
}
