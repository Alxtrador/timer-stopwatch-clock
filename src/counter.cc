#include "counter.h"

#include "SDL2/SDL.h"
#include "SDL2/SDL_video.h"
#include "SDL2/SDL_image.h"

#include <array>
#include <vector>

#include "constants.h"

Counter::Counter(SDL_Renderer *renderer, int width)
    : textures_(init_textures(renderer)), width_(width) {}

Counter::~Counter() {
    for (SDL_Texture *texture: textures_) {
        SDL_DestroyTexture(texture);
    }
}

std::array<SDL_Texture*, 10> Counter::init_textures(SDL_Renderer *renderer) {
    return std::array<SDL_Texture*, 10>{
        IMG_LoadTexture(renderer, "Assets/Images/Zero.png"),
        IMG_LoadTexture(renderer, "Assets/Images/One.png"),
        IMG_LoadTexture(renderer, "Assets/Images/Two.png"),
        IMG_LoadTexture(renderer, "Assets/Images/Three.png"),
        IMG_LoadTexture(renderer, "Assets/Images/Four.png"),
        IMG_LoadTexture(renderer, "Assets/Images/Five.png"),
        IMG_LoadTexture(renderer, "Assets/Images/Six.png"),
        IMG_LoadTexture(renderer, "Assets/Images/Seven.png"),
        IMG_LoadTexture(renderer, "Assets/Images/Eight.png"),
        IMG_LoadTexture(renderer, "Assets/Images/Nine.png")
    };
}

std::vector<int> Counter::digits(Uint64 value) {
    std::vector<int> result;
    while (value != 0) {
        int digit = value % 10;
        result.push_back(digit);
        value /= 10;
    }
    return result;
}

void Counter::draw(SDL_Renderer *renderer, Uint64 value) {
    std::vector<int> digits = Counter::digits(value);
    for (int i = 0; i < int(digits.size()); i++) {
        int digit = digits[i];
        SDL_Texture *digit_texture = textures_[digit];
        SDL_Rect size;
        size.x = width_ - (DIGIT_WIDTH * (i + 1) + (i * DIGIT_SPACING));
        size.y = 0;
        size.w = DIGIT_WIDTH;
        size.h = DIGIT_HEIGHT;
        SDL_RenderCopy(renderer, digit_texture, NULL, &size);
    }
    SDL_RenderPresent(renderer);
}

void Counter::run(SDL_Renderer *renderer) {
    SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255);
    SDL_RenderClear(renderer);
    SDL_RenderPresent(renderer);

    bool quit = false;
    Uint64 current_time = SDL_GetTicks64();
    Uint64 target_time = current_time + 1000;
    while (!quit) {
        SDL_Event event;
        while (SDL_PollEvent(&event)) {
            switch (event.type) {
                case SDL_QUIT:
                    quit = true;
                    break;
                default:
                    break;
            }
        }

        Uint64 passed_time = SDL_GetTicks64() - current_time;
        if (passed_time > 1000) {
            draw(renderer, target_time / 1000);
            target_time += 1000;
            current_time = SDL_GetTicks64();
        }

        //Introduce a short delay to avoid 100% usage of a cpu core
        SDL_Delay(1);
    }
}
