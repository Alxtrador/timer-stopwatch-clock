#ifndef APP_ENGINE_H_
#define APP_ENGINE_H_

#include "SDL2/SDL.h"

class AppEngine {
    public:
        AppEngine() {};
        virtual ~AppEngine() {};
        virtual void run(SDL_Renderer *renderer) = 0;
};

#endif
