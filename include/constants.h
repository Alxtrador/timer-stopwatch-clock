#ifndef CONSTANTS_H_
#define CONSTANTS_H_

constexpr int DIGIT_WIDTH = 124;
constexpr int DIGIT_HEIGHT = 217;
constexpr int DOT_WIDTH = 34;
constexpr int DOT_HEIGHT = 34;
constexpr int DIGIT_SPACING = 10;
constexpr int COLON_SPACING = 50;

#endif
