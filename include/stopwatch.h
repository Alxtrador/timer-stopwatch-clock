#ifndef STOPWATCH_H_
#define STOPWATCH_H_

#include "app_engine.h"

#include "SDL2/SDL.h"
#include "SDL2/SDL_video.h"
#include "SDL2/SDL_image.h"

#include <array>

class Stopwatch : public AppEngine {
    public:
        Stopwatch(SDL_Renderer *renderer, int width, int height);
        ~Stopwatch();
        void run(SDL_Renderer *renderer);

    private:
        void draw(SDL_Renderer *renderer, Uint64 value);
        std::array<SDL_Texture*, 11> init_textures(SDL_Renderer *renderer);

        std::array<SDL_Texture*, 11> textures_;
        int width_;
        int height_;
};

#endif
