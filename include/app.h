#ifndef APP_H_
#define APP_H_

#include "SDL2/SDL.h"
#include "SDL2/SDL_video.h"

#include <memory>

#include "app_engine.h"

enum Mode {
    eTimer,
    eStopwatch,
    eClock,
    eCounter
};

class App {
    public:
        App(int width, int height, Mode mode, int timer_count);
        ~App();
        App(const App&) = delete;
        App& operator=(const App&) = delete;
        void run();
        int width_;
        int height_;
        int mode_;

    private:
        void init_view();
        std::unique_ptr<AppEngine> create_engine(Mode mode, int timer_count);

        SDL_Window *window_;
        SDL_Renderer *renderer_;
        std::unique_ptr<AppEngine> engine_;
};

#endif
