#ifndef TIMER_H_
#define TIMER_H_

#include "SDL2/SDL.h"

#include <array>

#include "app_engine.h"
#include "audio_data.h"

class Timer : public AppEngine {
    public:
        Timer(SDL_Renderer *renderer, int width_, int count);
        ~Timer();
        void run(SDL_Renderer *renderer);

    private:
        void draw(SDL_Renderer *renderer);
        std::array<SDL_Texture*, 11> init_textures(SDL_Renderer *renderer);
        AudioData init_audio(std::string audio_file);

        std::array<SDL_Texture*, 11> textures_;
        AudioData audio_data_;
        int width_;
        int count_;
};

#endif