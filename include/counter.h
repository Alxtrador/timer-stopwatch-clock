#ifndef COUNTER_H_
#define COUNTER_H_

#include "SDL2/SDL.h"
#include "SDL2/SDL_video.h"

#include <array>
#include <vector>

#include "app_engine.h"

class Counter : public AppEngine {
    public:
        Counter(SDL_Renderer *renderer, int width);
        ~Counter();
        void run(SDL_Renderer *renderer);

    private:
        void draw(SDL_Renderer *renderer, Uint64 value);
        std::array<SDL_Texture*, 10> init_textures(SDL_Renderer *renderer);
        std::vector<int> digits(Uint64 value);

        std::array<SDL_Texture*, 10> textures_;
        int width_;
};

#endif
