#ifndef AUDIO_DATA_
#define AUDIO_DATA_

#include <string>

#include "SDL2/SDL.h"

class AudioData {
    public:
        AudioData(std::string audio_file);
        ~AudioData();
        AudioData(const AudioData&) = delete;
        AudioData& operator=(const AudioData&) = delete;
        void play_audio();

    private:
        SDL_AudioSpec spec_;
        Uint8 *audio_buf_;
        Uint32 audio_len_;
        SDL_AudioDeviceID device_id_;
};

#endif