#ifndef CLOCK_H_
#define CLOCK_H_

#include "SDL2/SDL.h"
#include "SDL2/SDL_video.h"

#include <array>
#include <vector>

#include "app_engine.h"

class Clock : public AppEngine {
    public:
        Clock(SDL_Renderer *renderer, int width);
        ~Clock();
        void run(SDL_Renderer *renderer);
   
    private:
        void draw(SDL_Renderer *renderer, char *time);
        std::array<SDL_Texture*, 11> init_textures(SDL_Renderer *renderer);

        std::array<SDL_Texture*, 11> textures_;
        int width_;
};

#endif
